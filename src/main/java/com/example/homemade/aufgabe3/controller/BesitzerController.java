package com.example.homemade.aufgabe3.controller;

import com.example.homemade.aufgabe3.model.Besitzer;
import com.example.homemade.aufgabe3.service.BesitzerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class BesitzerController {
    @Autowired
    BesitzerService besitzerService;
    private final String BASE_PATH = "/besitzer";
    @GetMapping(BASE_PATH)
    public List<Besitzer> getAllBesitzer(){return besitzerService.getAll();}
    @GetMapping(BASE_PATH+"/{buid}")
    public Besitzer getBesitzer(@PathVariable(value="buid")Long buid){
        return besitzerService.get(buid);
    }
    @DeleteMapping(BASE_PATH+"/{buid}")
    public ResponseEntity<String> deleteBesitzer(@PathVariable(value = "buid") Long buid){
        besitzerService.delete(buid);
        return ResponseEntity.ok("Besitzer successfully deleted");
    }
    @PutMapping(BASE_PATH+"/{buid}")
    public Besitzer updateBesitzer(@PathVariable(value = "buid")Long buid, @Valid @RequestBody Besitzer besitzer){
        Besitzer besitzer1=new Besitzer();
        besitzer1.setBname(besitzer.getBname());
        return besitzerService.update(buid,besitzer1);
    }
    @PostMapping(BASE_PATH)
    public Besitzer addBesitzer(@Valid@RequestBody Besitzer besitzer){
        return besitzerService.save(besitzer);
    }
}
