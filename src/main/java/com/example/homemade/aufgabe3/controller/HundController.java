package com.example.homemade.aufgabe3.controller;

import com.example.homemade.aufgabe3.model.Besitzer;
import com.example.homemade.aufgabe3.model.Hund;
import com.example.homemade.aufgabe3.service.BesitzerService;
import com.example.homemade.aufgabe3.service.HundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class HundController {
    @Autowired
    HundService hundService;
    @Autowired
    BesitzerService besitzerService;
    private final String hundePfad="/hund";

    @GetMapping(hundePfad)
    public List<Hund> getAllHunde(){
        return hundService.getAll();
    }

    @GetMapping({hundePfad + "/{hid}", "besitzer/{buid}/hund/{hid}"})
    public Hund getHund(@PathVariable Long hid){
        return hundService.get(hid);
    }

    @GetMapping("besitzer/{buid}/hund")
    public List<Hund> getAllHundFromBesitzer(@PathVariable Long buid){
       return hundService.getAllHundFromBesitzer(buid);

    }
    @DeleteMapping({hundePfad + "/{hid}", "besitzer/{buid}/hund/{hid}"})
    public ResponseEntity<String> deleteHund(@PathVariable Long hid){
        hundService.delete(hid);
        return ResponseEntity.ok("Hund deleted");
    }
    @PutMapping("besitzer/{buid}/hund/{hid}")
    public Hund updateHund(@PathVariable Long buid,@PathVariable Long hid, @Valid@RequestBody Hund wuffi){
        Hund hund=hundService.get(hid);
        hund.setName(wuffi.getName());
        hund.setRasse(wuffi.getRasse());
        hund.setBesitzer(besitzerService.get(buid));
        return hundService.update(hid,hund);
    }
    @PostMapping("/besitzer/{buid}/hund")
    public Hund createHund(@PathVariable Long buid, @Valid@RequestBody Hund hund){
        Besitzer besitzer=besitzerService.get(buid);
        Hund wuff= new Hund();
        wuff.setBesitzer(besitzer);
        wuff.setName(hund.getName());
        wuff.setRasse(hund.getRasse());
        return hundService.save(wuff,besitzer);
    }
}
