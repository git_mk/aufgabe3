package com.example.homemade.aufgabe3.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="besitzer")
public class Besitzer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long buid;
    @NotNull
    private String bname;

    public Long getBuid() {
        return buid;
    }

    public void setBuid(Long buid) {
        this.buid = buid;
    }

    public String getBname() {
        return bname;
    }

    public void setBname(String bname) {
        this.bname = bname;
    }
}
