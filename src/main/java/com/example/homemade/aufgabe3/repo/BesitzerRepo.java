package com.example.homemade.aufgabe3.repo;

import com.example.homemade.aufgabe3.model.Besitzer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BesitzerRepo extends JpaRepository<Besitzer,Long> {
}
