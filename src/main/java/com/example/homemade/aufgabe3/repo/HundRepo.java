package com.example.homemade.aufgabe3.repo;

import com.example.homemade.aufgabe3.model.Hund;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HundRepo extends JpaRepository<Hund,Long> {
}
