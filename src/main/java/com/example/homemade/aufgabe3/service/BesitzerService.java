package com.example.homemade.aufgabe3.service;

import com.example.homemade.aufgabe3.exceptions.ResourceNotFoundException;
import com.example.homemade.aufgabe3.model.Besitzer;
import com.example.homemade.aufgabe3.repo.BesitzerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BesitzerService {
    @Autowired
    BesitzerRepo besitzerRepo;
    public List<Besitzer> getAll(){
        if(besitzerRepo.findAll().isEmpty()){
            throw new ResourceNotFoundException("Es existieren keine Besitzer.");
        }
        return besitzerRepo.findAll();
    }
    public Besitzer save(Besitzer besitzer){
        return besitzerRepo.save(besitzer);
    }
    public Besitzer get(Long buid){
        return besitzerRepo.findById(buid).get();
    }
    public void delete(Long buid){
        besitzerRepo.deleteById(buid);
    }
    public Besitzer update(Long buid,Besitzer besitzer){
        Besitzer alterBesitzer=besitzerRepo.findById(buid).get();
        alterBesitzer.setBname(besitzer.getBname());
        return besitzerRepo.save(alterBesitzer);
    }
}
