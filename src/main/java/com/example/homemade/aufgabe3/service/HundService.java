package com.example.homemade.aufgabe3.service;

import com.example.homemade.aufgabe3.exceptions.ResourceNotFoundException;
import com.example.homemade.aufgabe3.model.Besitzer;
import com.example.homemade.aufgabe3.model.Hund;
import com.example.homemade.aufgabe3.repo.HundRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class HundService {
    @Autowired
    HundRepo hundRepo;
    public List<Hund> getAll(){
        if(hundRepo.findAll().isEmpty()){
            throw new ResourceNotFoundException("Es existieren keine Hunde.");
        }
        return hundRepo.findAll();
    }
    public Hund get(Long hid){
        return hundRepo.findById(hid).get();
    }
    public Hund save(Hund wuff, Besitzer besitzer){
        Hund hund=new Hund();
        hund.setName(wuff.getName());
        hund.setRasse(wuff.getRasse());
        hund.setBesitzer(besitzer);
        return hundRepo.save(hund);
    }
    public void delete(Long hid){
        hundRepo.deleteById(hid);
    }
    public Hund update(Long hid,Hund wuff){
      Hund bello=hundRepo.findById(hid).get();
      bello.setName(wuff.getName());
      bello.setRasse(wuff.getRasse());
      bello.setBesitzer(wuff.getBesitzer());
      return hundRepo.save(bello);
    }
    public List<Hund>getAllHundFromBesitzer(Long buid){
        List<Hund> liste=hundRepo.findAll();
        List<Hund>  hoho=new ArrayList<>();
        for(Hund hund:liste){
            if(hund.getBesitzer().getBuid()==buid)
                hoho.add(hund);
        }
        return hoho;
    }
}
